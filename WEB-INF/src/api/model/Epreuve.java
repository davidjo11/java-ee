package api.model;

import javax.persistence.Entity;

@Entity
public class Epreuve {
	
	private int id;
	
	private String jour;
	
	public Epreuve(){}
	
	public Epreuve(String jour){
		this.jour = jour;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJour() {
		return jour;
	}

	public void setJour(String jour) {
		this.jour = jour;
	}
}
