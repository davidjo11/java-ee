package api.model;

public enum Status {
	PREVU,
	ANNULE,
	PROVISOIRE,
	NON_OFFICIEL,
	OFFICIEL
}
