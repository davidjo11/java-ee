package jdbc;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestJDBC implements ITestJDBC {

	Connection conn;

	public Connection getConnection() {
		return conn;
	}

	// JDBC1 ******************************
	public void connect() throws SQLException, ClassNotFoundException, NullPointerException {
		Class.forName("org.sqlite.JDBC");
		conn = DriverManager.getConnection("jdbc:sqlite:data.db");
		//    conn = DriverManager.getConnection("jdbc:sqlite:C:\\Users\\David\\Documents\\GitProjects\\Master-e-Services\\IFI\\JavaEE\\tomcat8\\webapps\\tp1\\data.db");
		if(conn == null)
			throw new NullPointerException();

		this.createTable();
		
		try{
			this.addMember("Dufrene", "Guillaume", "guillaume.dufrene@webpulser.com", "test");
		}
		catch(Exception e){
			
		}

	}

	private void createTable() throws SQLException{
		Statement statement = conn.createStatement();
		String query = "CREATE TABLE IF NOT EXISTS MEMBER ("
				+ "nom    varchar(80),"
				+ "prenom varchar(80),"
				+ "email  varchar(100),"
				+ "pwd    varchar(45)"
				+ ");";
		statement.executeUpdate(query);
	}

	public boolean checkPasswordBasic( String email, String pwd ) throws SQLException {
		Statement statement = conn.createStatement();
		statement.setQueryTimeout(30);  // set timeout to 30 sec.
		// TODO: Change the request to search for member
		PreparedStatement pre = conn.prepareStatement( "SELECT email, pwd FROM member WHERE email = ? AND pwd = ?");
		// TODO: Use ResultSet to check if password match
		pre.setString(1,email);
		pre.setString(2,pwd);

		ResultSet rs = pre.executeQuery();

		if(!rs.next())
			return false;

		String m = rs.getString("email");
		String p = rs.getString("pwd");

		return m != null && p != null;
	}

	// JDBC2 ******************************
	public boolean exists(String email) throws SQLException {
		// TODO: implement this function
		Statement statement = conn.createStatement();
		statement.setQueryTimeout(30);  // set timeout to 30 sec.
		// TODO: Change the request to search for member
		PreparedStatement pre = conn.prepareStatement("SELECT email FROM member WHERE member.email = ?");
		pre.setString(1, email);
		ResultSet rs = pre.executeQuery();
		if(!rs.next())
			return false;
		return rs.getString("email").equals(email);
	}

	public void addMember(String lastname, String firstname, String email, String password) throws SQLException {
		// TODO: implement this function
		Statement statement = conn.createStatement();
		statement.setQueryTimeout(30);  // set timeout to 30 sec.
		// TODO: Change the request to search for member

		if(exists(email))
			throw new SQLException();

		PreparedStatement pre = conn.prepareStatement("INSERT INTO member values(?,?,?,?)");
		pre.setString(1, lastname);
		pre.setString(2, firstname);
		pre.setString(3, email);
		pre.setString(4, this.sha1(password));

		pre.execute();
	}

	public boolean checkPasswordWithSum( String email, String pwd ) throws SQLException {
		// TODO: implement this function
		Statement statement = conn.createStatement();
		statement.setQueryTimeout(30);  // set timeout to 30 sec.

		PreparedStatement pre = conn.prepareStatement("SELECT * from member WHERE email = ?");
		pre.setString(1, email);
		ResultSet rs = pre.executeQuery();
		if(!rs.next())
			return false;

		String res = sha1(pwd);

		return res.equals(rs.getString("pwd"));
	}

	private static String sha1(String password) {
		MessageDigest md = null;
		try {
			md = MessageDigest.getInstance("SHA-1");
		} catch(NoSuchAlgorithmException e) {
			e.printStackTrace();
			return "*****";
		}
		byte[] sha1sum = md.digest(password.getBytes());
		StringBuffer sb = new StringBuffer();
		for( int i = 0; i < sha1sum.length; i++ )
			sb.append( String.format("%02x", sha1sum[i]) );
		return sb.toString();
	}

	public static void main(String argv[]) throws Exception {
		TestJDBC db = new TestJDBC();
		db.connect();
		System.out.println( "Test du mot de passe");
		System.out.println(
				" ==> " + db.checkPasswordBasic( "guillaume.dufrene@webpulser.com", "test" )
				+ "\n==>" + db.checkPasswordWithSum( "guillaume.dufrene@webpulser.com", "test" )
				);
		db.disconnect();
	}

	public void disconnect() throws SQLException {
		// TODO Auto-generated method stub
		this.conn.close();
	}

}
