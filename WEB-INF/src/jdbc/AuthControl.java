package jdbc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/auth")
public class AuthControl extends HttpServlet {

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		List<String> params = new ArrayList<String>();

		TestJDBC test = null;
		Enumeration en = req.getParameterNames();
		String s = "";
		String error = "<div class=\"alert alert-danger\" role=\"alert\">Login ou mot de passe incorrect.</div>";
		while(en.hasMoreElements()){
			s = (String) en.nextElement();
			if(s.equals("email") || s.equals("password") || s.equals("remember")){
				params.add(s);
			}
		}
		System.out.println(params.size());
		if(params.size() < 2 && params.size() > 3){
			req.setAttribute("error", error);
			req.getRequestDispatcher("/login.jsp").forward(req, resp);
//			System.out.println("Nombre de param. incorrect."); 
		}
		else {
			String mail = req.getParameter("email") + "",
					pwd = req.getParameter("password") +"",
					remember = req.getParameter("remember");
			test = new TestJDBC();
			try{
				test.connect();
				if(test.exists(mail) && test.checkPasswordWithSum(mail, pwd)){
					req.setAttribute("pseudo", mail.substring(0, mail.indexOf("@")));
					req.getRequestDispatcher("/welcome.jsp").forward(req, resp);
//					System.out.println("Connexion réussie.");
				}
				else {
					req.setAttribute("error", error);
					req.setAttribute("email", mail);
					req.getRequestDispatcher("/login.jsp").forward(req, resp);
//					System.out.println("Les infos de connexion sont incorrects.");
				}
				test.disconnect();
			}
			catch(Exception e){
				e.printStackTrace();
			}

		}
		return;
	}
}
