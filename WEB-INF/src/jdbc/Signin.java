package jdbc;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/signin")
public class Signin extends HttpServlet {
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		List<String> params = new ArrayList<String>();

		TestJDBC test = null;
		Enumeration en = req.getParameterNames();
		String s = "";
		String error = "<div class=\"alert alert-danger\" role=\"alert\">Cette adresse mail est d�j� utilis�e.</div>";
		while(en.hasMoreElements()){
			s = (String) en.nextElement();
			if(s.equals("email") || s.equals("password") || s.equals("firstname") || s.equals("lastname")){
				params.add(s);
			}
		}
		System.out.println(params.size());
		if(params.size() != 4 ){
			req.setAttribute("error", error);
			req.getRequestDispatcher("/register.jsp").forward(req, resp);
//			System.out.println("Nombre de param. incorrect."); 
		}
		else {
			String mail = req.getParameter("email"),
					pwd = req.getParameter("password"),
					first = req.getParameter("firstname"),
					last = req.getParameter("lastname");
			
			test = new TestJDBC();
			try{
				test.connect();
				if(!test.exists(mail)){	
					test.addMember(last, first, mail, pwd);
					if(!test.exists(mail)){
						System.out.println(mail + " non ajouté!");
						throw new SQLException();
					}
					req.setAttribute("pseudo", first + " " + last);
					req.getRequestDispatcher("/welcome.jsp").forward(req, resp);
//					System.out.println("Connexion réussie.");
				}
				else {
					req.setAttribute("error", error);
					req.setAttribute("first", first + "");
					req.setAttribute("last", last + "");
					req.getRequestDispatcher("/register.jsp").forward(req, resp);
//					System.out.println("Les infos de connexion sont incorrects.");
				}
				test.disconnect();
			}
			catch(Exception e){
				e.printStackTrace();
			}

		}
		return;

	}
}