<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"
	import="java.util.Locale,java.util.Date,java.text.SimpleDateFormat,java.util.List,java.util.ArrayList"%>
<%!String action = "/tp1/signin";%>

<%
	String first = "",
	last = "",
	classCss = "";
	if (request.getAttribute("first") != null) {
		first = (String) request.getAttribute("first");
	}
	if (request.getAttribute("last") != null) {
		last = (String) request.getAttribute("last");
	}
%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="http://getbootstrap.com/favicon.ico">

<title>Register Now!</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
	integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
	crossorigin="anonymous">

<!-- Bootstrap core CSS -->
<link href="connect_fichiers/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="connect_fichiers/signin.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="http://getbootstrap.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="connect_fichiers/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
	<%!List<String> comments = new ArrayList<String>();%>

	<p>
		<br />
	</p>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="page-header" style="margin-top: 5px;">
						<h3>
							<span class="glyphicon glyphicon-log-in"></span> Register now !
						</h3>
					</div>
					<form class="form-horizontal" role="form" method="post"
						action="<%= action %>">
						<%
							if (request.getAttribute("error") != null) {
								classCss = "has-error";
								out.println(((String) request.getAttribute("error")));
							}
						%>
						<div class="form-group">
							<label for="inputfn3" class="col-sm-2 control-label">Firstname</label>
							<div class="col-sm-10">
								<div class="input-group">
									<span class="input-group-addon"> <span
										class="glyphicon glyphicon-user"></span>
									</span> <input type="text" required class="form-control" id="inputfn3"
										placeholder="Firstname" name="firstname" value="<%= first %>">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="inputfn3" class="col-sm-2 control-label">Lastname</label>
							<div class="col-sm-10">
								<div class="input-group">
									<span class="input-group-addon"> <span
										class="glyphicon glyphicon-user"></span>
									</span> <input type="text" required class="form-control" id="inputfn3"
										placeholder="LASTNAME" name="lastname" value="<%= last %>">
								</div>
							</div>
						</div>
						<div class="form-group <%= classCss %>">
							<label for="inputEmail3" class="col-sm-2 control-label">Email</label>
							<div class="col-sm-10">
								<div class="input-group">
									<span class="input-group-addon">@</span> <input type="email"
										required class="form-control" id="inputEmail3"
										placeholder="Email" name="email">
								</div>
							</div>
						</div>
						<div class="form-group">
							<label for="inputPassword3" class="col-sm-2 control-label">Password</label>
							<div class="col-sm-10">
								<div class="input-group">
									<span class="input-group-addon"> <span
										class="glyphicon glyphicon-asterisk"></span>
									</span> <input type="password" class="form-control" required
										id="inputPassword3" placeholder="Password" name="password">
								</div>
							</div>
						</div>
						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-10">
								<button type="submit" class="btn btn-primary">Signin</button>
								<br />
								<br /> If you already have an account, <a href="/tp1/login.jsp">login
									here</a>!
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="col-md-2"></div>
	</div>

	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<!-- Include all compiled plugins (below), or include individual files as needed -->
	<script
		src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

</body>
</html>
