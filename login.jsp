<%! String action = "/tp1/auth"; %>

<%! String mail = ""; %>

<% if(request.getAttribute("email") != null){
		mail = (String) request.getAttribute("email");
	}
 %>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="http://getbootstrap.com/favicon.ico">

<title>Login</title>

<!-- Bootstrap core CSS -->
<link href="connect_fichiers/bootstrap.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="connect_fichiers/signin.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]><script src="http://getbootstrap.com/assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="connect_fichiers/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

	<div class="container">
		<form class="form-signin" role="form" method="post"
			action="<%= action %>">
			<h2 class="form-signin-heading">Please sign in</h2>
			<% if(request.getAttribute("error") != null){
			out.println(((String) request.getAttribute("error")));
			}
		%>
			<label for="inputEmail" class="sr-only">Email address</label> <input
				id="inputEmail" class="form-control"
				placeholder="Votre adresse mail" required="" autofocus=""
				type="email" name="email" value="<%= mail %>"> <label
				for="inputPassword" class="sr-only">Password</label> <input
				id="inputPassword" class="form-control" placeholder="Password"
				required="" type="password" name="password">
			<div class="checkbox">
				<label> <input value="0" type="checkbox" name="remember">
					Remember me</input>
				</label>
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign
				in</button>
			<div>
				If you don't have an account yet, <a href="/tp1/register.jsp">register
					now</a>!
			</div>
		</form>

	</div>
	<!-- /container -->


	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="connect_fichiers/ie10-viewport-bug-workaround.js"></script>


</body>
</html>